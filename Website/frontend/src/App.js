import React, {useState} from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Home from "./layouts/home/Home";
import Admin from "./layouts/admin/Admin";
import Login from "./layouts/login/Login";
import Register from "./layouts/register/Register";

import * as Realm from "realm-web";

function App() {
    const REALM_APP_ID = "amigos-lgryp";
    const app = new Realm.App({ id: REALM_APP_ID});

    const [user, setUser] = useState(app.currentUser);

    return (
        <BrowserRouter>
            <Switch>
                <Route path="/home" render={(props) => <Home {...props} />}/>
                <Route path="/admin" render={(props) => <Admin {...props} user={user} setUser={setUser} app={app} />}/>
                <Route path="/login" render={(props) => <Login {...props} user={user} setUser={setUser} app={app} />}/>
                <Route path="/register" render={(props) => <Register {...props} app={app} />}/>
                <Redirect to="/home"/>
            </Switch>
        </BrowserRouter>
    )
}

export default App;