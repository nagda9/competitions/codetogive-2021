import React, {useState, useRef, useEffect} from "react";

import "layouts/admin/css/admin.css";
import "layouts/admin/css/scss/paper-dashboard.scss?v=1.3.0";
import "perfect-scrollbar/css/perfect-scrollbar.css";

import PerfectScrollbar from "perfect-scrollbar";
import {Redirect, Route, Switch, useLocation} from "react-router-dom";

import Navbar from "./components/Navbar/Navbar.js";
import Footer from "./components/Footer.js";
import Sidebar from "./components/Sidebar.js";

import routes from "./components/Navbar/routes";

let ps;

function Dashboard(props) {
    const [backgroundColor] = useState("black");
    const [activeColor] = useState("info");
    const mainPanel = useRef();
    const location = useLocation();

    useEffect(() => {
        if (props.user) {
            if (navigator.platform.indexOf("Win") > -1) {
                ps = new PerfectScrollbar(mainPanel.current);
                document.body.classList.toggle("perfect-scrollbar-on");
            }

            return function cleanup() {
                if (navigator.platform.indexOf("Win") > -1) {
                    ps.destroy();
                    document.body.classList.toggle("perfect-scrollbar-on");
                }
            };
        }
    });

    useEffect(() => {
        if (props.user) {
            mainPanel.current.scrollTop = 0;
            document.scrollingElement.scrollTop = 0;
        }
    }, [location]);

    return (
        <div className="wrapper">
            {props.user ? (
                <div>
                    <Sidebar
                        {...props}
                        routes={routes}
                        bgColor={backgroundColor}
                        activeColor={activeColor}
                    />
                    <div className="main-panel" ref={mainPanel}>
                        <Navbar {...props} />
                        <Switch>
                            {routes.map((prop, key) => {
                                return (
                                    <Route
                                        path={prop.layout + prop.path}
                                        component={prop.component}
                                        key={key}
                                    />
                                );
                            })}
                            <Redirect to="/admin/dashboard"/>
                        </Switch>
                        <Footer fluid/>
                    </div>
                </div>
            ) : <Redirect to="/login"/>}
        </div>
    );
}

export default Dashboard;
