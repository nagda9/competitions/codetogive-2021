import React from "react";
import {Container, Row} from "reactstrap";
import PropTypes from "prop-types";

function Footer(props) {
    return (
        <footer className={"footer" + (props.default ? " footer-default" : "")}>
            <Container fluid={props.fluid}>
                <Row>
                    <div className="credits ml-auto">
                        <a href="https://www.amigosagyerekekert.hu/amigosonline/" target="_blank" rel="noreferrer">
                            <div className="copyright">
                                Amigos &copy; {1900 + new Date().getYear()}
                            </div>
                        </a>
                    </div>
                </Row>
            </Container>
        </footer>
    );
}

Footer.propTypes = {
    default: PropTypes.bool,
    fluid: PropTypes.bool,
};

export default Footer;
