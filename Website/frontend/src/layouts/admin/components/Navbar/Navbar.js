import React, {useEffect, useState, useRef} from "react";
import {useLocation, useHistory} from "react-router-dom";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Container
} from "reactstrap";

import routes from "./routes";

function Header(props) {
    const [isOpen, setIsOpen] = useState(false);
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [color, setColor] = useState("transparent");
    const sidebarToggle = useRef();
    const location = useLocation();

    const toggle = () => {
        if (isOpen) {
            setColor("transparent");
        } else {
            setColor("dark");
        }
        setIsOpen(!isOpen);
    };

    const dropdownToggle = () => {
        setDropdownOpen(!dropdownOpen);
    };

    const getBrand = () => {
        let brandName = "Default Brand";
        routes.map((prop) => {
            if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
                brandName = prop.name;
            }
            return null;
        });
        return brandName;
    };
    const openSidebar = () => {
        document.documentElement.classList.toggle("nav-open");
        sidebarToggle.current.classList.toggle("toggled");
    };
    // function that adds color dark/transparent to the navbar on resize (this is for the collapse)
    const updateColor = () => {
        if (window.innerWidth < 993 && isOpen) {
            setColor("dark");
        } else {
            setColor("transparent");
        }
    };

    useEffect(() => {
        window.addEventListener("resize", updateColor.bind(this));
    });

    useEffect(() => {
        if (
            window.innerWidth < 993 &&
            document.documentElement.className.indexOf("nav-open") !== -1
        ) {
            document.documentElement.classList.toggle("nav-open");
            sidebarToggle.current.classList.toggle("toggled");
        }
    }, [location]);

    const history = useHistory();
    const logout = async () => {
        await props.app.removeUser(props.user)
            .then(() => {
                console.log(props.app.currentUser);
                props.setUser(props.app.currentUser);
            })
            .then(() => history.push("/login"))
            .catch(() => "failed to log out");
    }

    return (
        // add or remove classes depending if we are on full-screen-maps page or not
        <Navbar
            color={
                props.location.pathname.indexOf("full-screen-maps") !== -1
                    ? "dark"
                    : color
            }
            expand="lg"
            className={
                props.location.pathname.indexOf("full-screen-maps") !== -1
                    ? "navbar-absolute fixed-top"
                    : "navbar-absolute fixed-top " +
                    (color === "transparent" ? "navbar-transparent " : "")
            }
        >
            <Container fluid>
                <div className="navbar-wrapper">
                    <div className="navbar-toggle">
                        <button
                            type="button"
                            ref={sidebarToggle}
                            className="navbar-toggler"
                            onClick={() => openSidebar()}
                        >
                            <span className="navbar-toggler-bar bar1"/>
                            <span className="navbar-toggler-bar bar2"/>
                            <span className="navbar-toggler-bar bar3"/>
                        </button>
                    </div>
                    <NavbarBrand>{getBrand()}</NavbarBrand>
                </div>
                <NavbarToggler onClick={toggle}>
                    <span className="navbar-toggler-bar navbar-kebab"/>
                    <span className="navbar-toggler-bar navbar-kebab"/>
                    <span className="navbar-toggler-bar navbar-kebab"/>
                </NavbarToggler>
                <Collapse isOpen={isOpen} navbar className="justify-content-end">
                    <Nav navbar>
                        <Dropdown nav isOpen={dropdownOpen} toggle={(e) => dropdownToggle(e)}>
                            <DropdownToggle caret nav>
                                <i className="nc-icon nc-settings-gear-65"/>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>Action</DropdownItem>
                                <DropdownItem>Another Action</DropdownItem>
                                <DropdownItem onClick={logout}>Log out</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </Nav>
                </Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;
