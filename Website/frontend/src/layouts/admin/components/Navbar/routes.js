import Dashboard from "layouts/admin/views/Dashboard";
import Math from "layouts/admin/views/Math";

let routes = [
    {
        path: "/dashboard",
        name: "Dashboard",
        icon: "nc-icon nc-bank",
        component: Dashboard,
        layout: "/admin",
    },
    {
        path: "/math",
        name: "Math",
        icon: "nc-icon nc-book-bookmark",
        component: Math,
        layout: "/admin",
    },
];

export default routes;
