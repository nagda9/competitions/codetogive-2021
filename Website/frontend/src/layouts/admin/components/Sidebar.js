import React, {useEffect, useRef} from "react";
import {NavLink} from "react-router-dom";
import {Nav} from "reactstrap";

import PerfectScrollbar from "perfect-scrollbar";

import logo from "../../../assets/img/logo_green.PNG";

let ps;

function Sidebar(props) {
    const sidebar = useRef();
    const activeRoute = (routeName) => {
        return props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    };

    useEffect(() => {
        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(sidebar.current, {
                suppressScrollX: true,
                suppressScrollY: false,
            });
        }
        return function cleanup() {
            if (navigator.platform.indexOf("Win") > -1) {
                ps.destroy();
            }
        };
    });

    return (
        <div className="sidebar" data-color={props.bgColor} data-active-color={"success"}>
            <div className="logo">
                <a href={"/admin/dashboard"} className="simple-text logo-mini">
                    <div className="logo-img">
                        <img src={logo} alt="react-logo"/>
                    </div>
                </a>
                <a href={"/admin/dashboard"} className="simple-text logo-normal">
                    Szia {props.user.id}
                </a>
            </div>
            <div className="sidebar-wrapper" ref={sidebar}>
                <Nav>
                    {props.routes.map((prop, key) => {
                        return (
                            <li className={activeRoute(prop.path)} key={key}>
                                <NavLink to={prop.layout + prop.path} className="nav-link" activeClassName="active">
                                    <i className={prop.icon}/>
                                    <p>{prop.name}</p>
                                </NavLink>
                            </li>
                        );
                    })}
                </Nav>
            </div>
        </div>
    );
}

export default Sidebar;
