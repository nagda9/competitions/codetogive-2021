import React from 'react';
import {Link} from "react-router-dom";
import {
    NavbarText
} from 'reactstrap';
import PropTypes from "prop-types"
import bg from '../../assets/img/amigos_fake_bg_mod.png';

const Home = () => {
    return (
        <div>
            <div>
                <Link to="/login" className="btn btn-primary" style={
                    {
                        marginTop: "25px",
                        marginLeft: "78.5%",
                        position: "absolute"
                    }
                }>Login</Link>
                <img src={bg} width={"100%"} alt="" />
            </div>

        </div>
    );
}

NavbarText.propTypes = {
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string])
    // pass in custom element to use
}

export default Home;