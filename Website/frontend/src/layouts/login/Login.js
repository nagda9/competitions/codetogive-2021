import React, {useState} from "react";
import {Redirect} from "react-router";
import {Link, useHistory} from "react-router-dom";
import * as Realm from "realm-web";
import arrow_back from "../../assets/img/arrow_back.svg";

function Login(props) {
    const [details, setDetails] = useState({email: "", password: ""});
    const [loginError, setLoginError] = useState("");
    const history = useHistory();

    const loginEmailPassword = async (e) => {
        e.preventDefault();
        // Create credentials
        const credentials = Realm.Credentials.emailPassword(details.email, details.password);
        await props.app.logIn(credentials)
            .then((new_user) => {
                props.setUser(new_user);
                console.log("Successfully logged in!")
                history.push("/admin");
            })
            .catch((err) =>{
                let loginError = err.toString().split(": ")[2];
                loginError = loginError.charAt(0).toUpperCase() + loginError.slice(1);
                loginError = loginError.split(" (")[0] + ".";
                setLoginError(loginError.toString());
                console.log("Login failed!");
            })
    }

    return (
        <div className="d-flex align-items-center justify-content-center vh-100">
            {props.user ? (
                <div>
                    <Redirect to="/admin" />
                </div>

            ) : (
                <div className="border"
                     style={{
                        padding: "20px",
                        minWidth: "30%",
                        maxWidth: "40%",
                    }}
                >
                    <form onSubmit={loginEmailPassword}>
                        <div className="form-inner">
                            <div className="d-flex align-items-center"
                                 style={{marginBottom: "10px"}}
                            >
                                <Link to="/home">
                                    <img src={arrow_back} alt="arrow_back" style={{paddingRight: "10px"}} />
                                </Link>
                                <h1 style={{margin: "0"}}>Login</h1>
                            </div>

                            <div><span style={{color: "red"}}>{loginError}</span></div>

                            <div className="form-group d-flex flex-column">
                                <label htmlFor="email" className="d-flex">Name:</label>
                                <input className="d-flex"
                                       type="email"
                                       name="email"
                                       id="email"
                                       value={details.email}
                                       onChange={e => setDetails({...details, email: e.target.value})}
                                />
                            </div>

                            <div className="form-group d-flex flex-column">
                                <label htmlFor="password" className="d-flex">password:</label>
                                <input className="d-flex"
                                       type="password"
                                       name="password"
                                       id="password"
                                       value={details.password}
                                       onChange={e => setDetails({...details, password: e.target.value})}
                                />
                            </div>
                            <input type="submit" className="btn btn-primary" value="LOG IN"/>
                            <Link to="/register" style={{
                                paddingLeft: "10px"
                            }}>Register</Link>
                        </div>
                    </form>
                    <Link to="/login">Forgot your password?</Link>
                </div>
            )}
        </div>
    );
}

export default Login;