import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import arrow_back from "../../assets/img/arrow_back.svg";

function Register(props) {
    const [details, setDetails] = useState({email: "", password: "", password_again: ""});
    const [registerError, setRegisterError] = useState({text: "", color: "red"});
    const [resendLog, setResendLog] = useState("");
    const [confirmationLog, setConfirmationLog] = useState({text: "", color: "red"});

    const registerEmailPassword = async (e) => {
        e.preventDefault();
        if(details.password === details.password_again) {
            await props.app.emailPasswordAuth.registerUser(details.email, details.password)
                .then(() => {
                    setRegisterError({text: "Success! A confirmation sent to your email address!", color: "green"});
                    setDetails({email: "", password: "", password_again: ""});
                })
                .catch((err) => {
                    let regError = err.toString().split(": ")[2];
                    regError = regError.charAt(0).toUpperCase() + regError.slice(1);
                    regError = regError.split(" (")[0] + ".";
                    setRegisterError({text: regError, color: "red"});
                })
        } else {
            setRegisterError({text: "Passwords does not match!", color: "red"});
        }
    }

    const resendConfirmationEmail = (e) => {
        e.preventDefault();
        if (details.email !== "") {
            props.app.emailPasswordAuth.resendConfirmationEmail(details.email)
                .then(() => setResendLog("Success"))
                .catch((err) => {
                    let resendLog = err.toString().split(": ")[2];
                    resendLog = resendLog.charAt(0).toUpperCase() + resendLog.slice(1);
                    resendLog = resendLog.split(" (")[0] + ".";
                    setResendLog(resendLog);
                })
        } else {
            console.log("There is no email to confirm!")
        }
    }

    const query = new URLSearchParams(window.location.search);

    useEffect(async () => {
        if (query.get("token") && query.get("tokenId")) {
            const token = query.get("token");
            const tokenId = query.get("tokenId");
            await props.app.emailPasswordAuth.confirmUser(token, tokenId)
                .then(() => {
                    setConfirmationLog({text: "Email successfully confirmed!", color: "green"});
                })
                .catch((err) => {
                    let confirmationLog = err.toString().split(": ")[2];
                    confirmationLog = confirmationLog.charAt(0).toUpperCase() + confirmationLog.slice(1);
                    confirmationLog = confirmationLog.split(" (")[0] + ".";
                    setConfirmationLog({text: confirmationLog, color: "red"});
                })
        }
    }, []);



    return (
        <div className="d-flex align-items-center justify-content-center vh-100">
            {props.location.pathname === "/register/confirmation" ? (
                <div className="border"
                     style={{
                         padding: "20px",
                         minWidth: "30%",
                         maxWidth: "40%",
                     }}
                >
                    {query.get("token") && query.get("tokenId")? (
                        <div>
                            <div><span style={{color: confirmationLog.color}}>{confirmationLog.text}</span></div>
                            <Link to="/login" className="btn btn-primary">Back</Link>
                        </div>
                    ) : (
                        <div>
                            <form onSubmit={resendConfirmationEmail}>
                                <div className="d-flex align-items-center"
                                     style={{marginBottom: "10px"}}
                                >
                                    <Link to="/register">
                                        <div style={{paddingRight: "10px"}}>
                                            <img src={arrow_back} alt="arrow_back" />
                                        </div>
                                    </Link>
                                    <h2 style={{margin: "0"}}>Resend confirmation email</h2>
                                </div>

                                <div className="form-group">
                                    <div><span style={{color: "red"}}>{resendLog}</span></div>
                                    <div className="form-group  d-flex flex-column">
                                        <label htmlFor="email">Email:</label>
                                        <input className="d-flex"
                                               type="email"
                                               name="email"
                                               id="email"
                                               value={details.email}
                                               onChange={e => setDetails({...details, email: e.target.value})}
                                        />
                                    </div>
                                    <input type="submit" className="btn btn-primary" value="Send"/>
                                </div>
                            </form>
                        </div>
                    )}
                </div>
            ) : (
                <div className="border"
                     style={{
                         padding: "20px",
                         minWidth: "30%",
                         maxWidth: "40%",
                     }}
                >

                    <form onSubmit={registerEmailPassword}>
                        <div className="form-inner">
                            <div className="d-flex align-items-center"
                                 style={{marginBottom: "10px"}}
                            >
                                <Link to="/login">
                                    <img src={arrow_back} alt="arrow_back" style={{paddingRight: "10px"}} />
                                </Link>
                                <h1 style={{margin: "0"}}>Register</h1>
                            </div>

                            <div><span style={{color: registerError.color}}>{registerError.text}</span></div>

                            <div className="form-group d-flex flex-column">
                                <label htmlFor="email">Email:</label>
                                <input className="d-flex"
                                       type="email"
                                       name="email"
                                       id="email"
                                       value={details.email}
                                       onChange={e => setDetails({...details, email: e.target.value})}
                                />
                            </div>

                            <div className="form-group d-flex flex-column">
                                <label htmlFor="password">Password:</label>
                                <input className="d-flex"
                                       type="password"
                                       name="password"
                                       id="password"
                                       value={details.password}
                                       onChange={e => setDetails({...details, password: e.target.value})}
                                />
                            </div>

                            <div className="form-group d-flex flex-column">
                                <label htmlFor="password">Password again:</label>
                                <input className="d-flex"
                                       type="password"
                                       name="password"
                                       value={details.password_again}
                                       onChange={e => setDetails({...details, password_again: e.target.value})}
                                />
                            </div>

                            <input type="submit"  className="btn btn-primary"  value="Register"/>
                        </div>
                    </form>
                    <Link to="/register/confirmation">Resend confirmation email</Link>
                </div>
            )}
        </div>
    );
}

export default Register;